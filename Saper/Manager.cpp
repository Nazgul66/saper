#include "Manager.h"
#include <iostream>


Manager::Manager()
{}


Manager::~Manager()
{}

void Manager::AddResult(std::pair<std::string, int> result)
{
	auto cmp = [](std::pair<std::string, int> const & a, std::pair<std::string, int> const & b)
	{
		return a.second != b.second ? a.second < b.second : a.first < b.first;
	};
	results.push_back(result);
	std::sort(results.begin(), results.end(), cmp);
}

void Manager::Save()
{
	std::ofstream of;
	of.open("table.ath", std::ios_base::binary);
	if(of.is_open() && of.good())
	{
		for(auto r : results)
		{
			/*of.write(r.first.c_str(), r.first.size());
			of.write((char*)r.second, sizeof(int));*/
			of << r.first.c_str();
			of << r.second;
		}
		of.close();
	}
}

void Manager::Load()
{
	std::ifstream of;
	of.open("table.ath", std::ios_base::binary);
	if(of.is_open() && of.good()){
		while(!of.eof())
		{
			char s[100];
			int points;
			of >> s;
			of >> points;
			std::pair<std::string, int>l(s, points);
			results.push_back(l);
		}
	}
}

void Manager::Show() const
{
	for(auto x : results)
		std::cout << x.first.c_str() << "\t" << x.second << std::endl;
}
