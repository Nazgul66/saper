#include "Input.h"



Input::Input()
{}

bool Input::Update(int & x, int & y)
{
	c = _getch();

	switch(c)
	{
		case Keyboard::UP: Up(y); break;
		case Keyboard::DOWN: Down(y); break;
		case Keyboard::LEFT: Left(x); break;
		case Keyboard::RIGHT: Right(x); break;
		case Keyboard::SPACE: return true; break;
		default: return false; break;
	}
}

void Input::Down(int & y)
{
	if((y + 1) < 10)
		++y;
}

void Input::Up(int & y)
{
	if((y - 1) >= 0)
		--y;
}

void Input::Left(int & x)
{
	if((x - 1) >= 0)
		--x;
}

void Input::Right(int & x)
{
	if((x + 1) < 9)
		++x;
}
