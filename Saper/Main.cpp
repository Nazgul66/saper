#include "Board.h"
#include "Manager.h"
#include "Player.h"
#include "Input.h"
#include <ctime>
#define unFail 0;


auto main(void)->int
{
	srand(std::time(nullptr));

	bool win = false;
	bool play = true;

	Board data;
	Board display;
	data.RandomNewMines(20);

	Input input;
	Player player;
	Manager manager;
	std::string name;
	std::cout << "Podaj imie: ";
	std::cin >> name;
	player.SetName(name);

	while(play)
	{
		display.Draw();
		if(input.Update(display.selected_X, display.selected_Y) == true)
		{
			if(display.Discover(data) == true)
				play = false;
		}
		if(display.IsOnlyBomb())
		{
			win = true;
			play = false;
		}
	}

	display.Draw();
	player.SetResult();
	std::cout << "\nTwoj czas to: " << player.Time();
	if(win)
	{
		manager.AddResult(player.Result());
		manager.Save();
	}
	Sleep(3000);
	system("cls");
	manager.Load();
	manager.Show();
	system("pause");
	return unFail;
}