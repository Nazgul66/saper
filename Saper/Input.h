#pragma once
#include <conio.h>
//#include <cstdlib>
class Input
{
public:
	Input();
	bool Update(int& x, int& y);
private:
	enum Keyboard
	{
		UP = 72, LEFT = 75, RIGHT = 77, DOWN = 80, SPACE = ' '
	};

	void Down(int& y);
	void Up(int& y);
	void Left(int& x);
	void Right(int& x);

	char c;
};

