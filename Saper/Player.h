#pragma once
#include <map>
#include <string>
#include <ctime>
class Player
{
public:
	Player();
	void SetName(std::string name);
	void SetResult();
	std::string Name()const;
	int Time()const;
	std::pair<std::string, int> Result()const;
private		:
	clock_t time = 0;

	std::string name;
	std::pair<std::string, int> result;
};

