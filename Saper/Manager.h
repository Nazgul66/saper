#pragma once
#include <map>
#include <vector>
#include <algorithm>
#include <fstream>
class Manager
{
public:
	Manager();
	~Manager();

	void AddResult(std::pair<std::string, int> result);
	void Save();
	void Load();
	void Show()const;
private:
	std::vector<std::pair<std::string, int>> results;
};

