#include "Board.h"

Board::Board()
{
	Clear();
}

void Board::Draw()const
{
	int w = 1;
	std::system("cls");
	for(size_t y = 0; y < height; ++y)
	{
		for(size_t i = 0; i < 2 * width + 1; ++i)
			std::cout << "-";

		std::cout << "\n|";
		for(size_t x = 0; x < width; ++x)
		{
			if(y == selected_Y && x == selected_X)
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | BACKGROUND_BLUE);
			else
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | BACKGROUND_BLUE);
			std::cout << board[y][x] << "|";
		}
		std::cout << "\n";
	}
	for(size_t i = 0; i < 2 * width + 1; ++i)
		std::cout << "-";
}

void Board::Clear()
{
	for(size_t y = 0; y < height; ++y)
	{
		for(size_t x = 0; x < width; ++x)
		{
			board[y][x] = ' ';
		}
	}
}

void Board::RandomNewMines(int mines)
{
	bombs = mines;

	Clear();

	while(bombs-- > 0)
	{
		int c = rand() % 90;
		int y = c / width;
		int x = c - y*width;
		board[y][x] = '*';
	}

	for(size_t y = 0; y < height; ++y)
	{
		for(size_t x = 0; x < width; ++x)
		{
			int w = 0;
			int h = 0;
			int yy = y - 2;
			int xx = x - 1;
			if(board[y][x] != '*')
			{
				bombs = 0;
				for(size_t h = 0; h < 3; ++h)
				{
					++yy;
					xx = x - 2;
					for(size_t w = 0; w < 3; ++w)
					{
						++xx;
						if(yy >= 0 && yy < height && xx >= 0 && xx < width)
						{
							if(board[yy][xx] == '*')
								++bombs;
						}
					}
				}
				board[y][x] = '0' + bombs;
			}
		}
	}
}

bool Board::Discover(const Board& board)
{
	this->board[selected_Y][selected_X] = board.board[selected_Y][selected_X];
	if(this->board[selected_Y][selected_X] == '*')
		return true;
	return 
		false;
}

void Board::SetBombs(int bombs)
{
	this->bombs = bombs;
}

bool Board::IsOnlyBomb() const
{
	return (freePlacesChecked == 90 - bombs) ? true : false;
}
