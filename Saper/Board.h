#pragma once
#include <Windows.h>
#include <iostream>
class Board
{
public:
	int selected_X = 0;
	int selected_Y = 0;

	Board();
	void Clear();
	void Draw()const;
	void RandomNewMines(int);
	bool Discover(const Board& board);
	void SetBombs(int);

	bool IsOnlyBomb()const;
private:
	int freePlacesChecked = 0;
	int width = 9;
	int height = 10;
	int bombs = 10;

	char board[10][9];
};
