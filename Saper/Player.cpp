#include "Player.h"



Player::Player()
{
	time = clock();
}

void Player::SetName(std::string name)
{
	this->name = name;
}

void Player::SetResult()
{
	time = clock() - time;
	result = std::pair<std::string, int>(name, time / CLOCKS_PER_SEC);
}

std::string Player::Name() const
{
	return name;
}

int Player::Time() const
{
	return result.second;
}

std::pair<std::string, int> Player::Result() const
{
	return result;
}


